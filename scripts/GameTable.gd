extends Spatial

var computerThreshold = 8
var whoWon = 0
var isItMidnight = 0
var maxPlayers = 0
var remainingRolls = 3
var hasShip = 0
var hasCapt = 0
var hasCrew = 0
var computerscore = 0
var player1score = 0
var player2score = 0
var player3score = 0
var player4score = 0
var diceA = 0
var diceB = 0
var diceC = 0
var diceD = 0
var diceE = 0
var takenA = 0
var takenB = 0
var takenC = 0
var takenD = 0
var takenE = 0
var tumbler = 3
var theCurrentPlayer = 0

func _ready():
	var new_y_scale = get_node("/root/global").my_viewport_scale
	var new_x_scale = new_y_scale
	var new_z_scale = new_y_scale
	
	if get_node("/root/global").actualScreenRatio < 1.4 :
		new_x_scale = new_y_scale * ((800/600) / get_node("/root/global").actualScreenRatio)
	elif get_node("/root/global").actualScreenRatio < 1.61 :
		new_x_scale = new_y_scale / (get_node("/root/global").actualScreenRatio/1.45)
	elif get_node("/root/global").actualScreenRatio < 1.76 :
		new_x_scale = new_y_scale / (get_node("/root/global").actualScreenRatio/1.54)
	else :
		new_x_scale = new_y_scale
	
	get_node("/root").get_children()[2].set_scale(Vector3(new_x_scale,new_y_scale,new_z_scale))
	# And set the HUD scale...
	get_node("ControlHUD").set_scale(Vector2(new_x_scale,new_y_scale))
	get_node("winControl").set_scale(Vector2(new_x_scale,new_y_scale))
	# print(get_node("/root").get_children()[1].get_scale()) # Reference only.
	_make_dice_random("LowPolyDiceA")
	_make_dice_random("LowPolyDiceB")
	_make_dice_random("LowPolyDiceC")
	_make_dice_random("LowPolyDiceD")
	_make_dice_random("LowPolyDiceE")
	if get_node("/root/global").computerPlayer == 1 :
		theCurrentPlayer = 0
	else :
		theCurrentPlayer = 1
	$winControl.hide()
	_setup_game()
	
func _setup_game():
	$ControlHUD.show()
	$winControl.hide()
	$pocketwatch.hide()
	maxPlayers = get_node("/root/global").numberOfPlayers
	computerscore = 0
	player1score = 0
	player2score = 0
	player3score = 0
	player4score = 0
	whoWon = 0
	isItMidnight = 0
	_next_player_setup()

func _next_player_setup():
	get_node("ControlHUD/HUDwhoseTurnLabel").set("whoseTurnIsIt", theCurrentPlayer)
	takenA = 0
	takenB = 0
	takenC = 0
	takenD = 0
	takenE = 0
	remainingRolls = 3
	get_node("ControlHUD/HUDrollsNumLabel").set("howManyRolls", remainingRolls)
	$ControlHUD/HUDkeepButton.hide()
	$ControlHUD/HUDrollButton.hide()
	hasShip = 0
	$pirateShip.hide()
	hasCapt = 0
	$pirateHat.hide()
	hasCrew = 0
	$piratecrewHat.hide()
	$ControlHUD/HUDrollButton.show()
	if theCurrentPlayer == 0:
		$computerTimer.start()

func _make_dice_random (diceName):
	var x = randi() % 365 + 1
	var y = randi() % 365 + 1
	var z = randi() % 365 + 1
	get_node(diceName).rotate_x(deg2rad(x))
	get_node(diceName).rotate_y(deg2rad(y))
	get_node(diceName).rotate_z(deg2rad(z))

func _spin_dice():
	if tumbler > 0 :
		tumbler = tumbler - 1
		#print (tumbler)
		var upAmmount = 4
		if takenA != 1:
			$LowPolyDiceA.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
			_make_dice_random("LowPolyDiceA")
		if takenB != 1:
			$LowPolyDiceB.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
			_make_dice_random("LowPolyDiceB")
		if takenC != 1:
			$LowPolyDiceC.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
			_make_dice_random("LowPolyDiceC")
		if takenD != 1:
			$LowPolyDiceD.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
			_make_dice_random("LowPolyDiceD")
		if takenE != 1:
			$LowPolyDiceE.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
			_make_dice_random("LowPolyDiceE")
		$spinTimer.start()
	else :
		$spinTimer.stop()
		$Timer.start()
	
func _roll_dice():
	$ControlHUD/HUDrollButton.hide()
	remainingRolls = remainingRolls - 1
	get_node("ControlHUD/HUDrollsNumLabel").set("howManyRolls", remainingRolls)
	tumbler = 3
	var upAmmount = 6
	if takenA != 1:
		$LowPolyDiceA.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
	if takenB != 1:
		$LowPolyDiceB.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
	if takenC != 1:
		$LowPolyDiceC.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
	if takenD != 1:
		$LowPolyDiceD.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
	if takenE != 1:
		$LowPolyDiceE.apply_impulse(Vector3(),Vector3(0,upAmmount,0))
	$spinTimer.start()

func _check_all_dice():
	$Timer.stop()
	#print ("dice check")
	#print (diceA, diceB, diceC, diceD, diceE)
	# Check for Ship
	if hasShip == 0 :
		if diceA == 6 :
			hasShip = 1
			$pirateShip.show()
			takenA = 1
		elif diceB == 6 :
			hasShip = 1
			$pirateShip.show()
			takenB = 1
		elif diceC == 6 :
			hasShip = 1
			$pirateShip.show()
			takenC = 1
		elif diceD == 6 :
			hasShip = 1
			$pirateShip.show()
			takenD = 1
		elif diceE == 6 :
			hasShip = 1
			$pirateShip.show()
			takenE = 1
	# Now check for captain
	if hasShip == 1 :
		if diceA == 5 :
			hasCapt = 1
			$pirateHat.show()
			takenA = 1
		elif diceB == 5 :
			hasCapt = 1
			$pirateHat.show()
			takenB = 1
		elif diceC == 5 :
			hasCapt = 1
			$pirateHat.show()
			takenC = 1
		elif diceD == 5 :
			hasCapt = 1
			$pirateHat.show()
			takenD = 1
		elif diceE == 5 :
			hasCapt = 1
			$pirateHat.show()
			takenE = 1
	# Now check for crew
	if hasCapt == 1 :
		if diceA == 4 :
			hasCrew = 1
			$piratecrewHat.show()
			takenA = 1
		elif diceB == 4 :
			hasCrew = 1
			$piratecrewHat.show()
			takenB = 1
		elif diceC == 4 :
			hasCrew = 1
			$piratecrewHat.show()
			takenC = 1
		elif diceD == 4 :
			hasCrew = 1
			$piratecrewHat.show()
			takenD = 1
		elif diceE == 4 :
			hasCrew = 1
			$piratecrewHat.show()
			takenE = 1
	# Now check cargo
	if hasCrew == 1:
		if remainingRolls > 0:
			$ControlHUD/HUDkeepButton.show()
		else:
			$ControlHUD/HUDkeepButton.hide()
		_update_score(diceA + diceB + diceC + diceD + diceE - 15)
	else :
		_update_score(0)
	
	if remainingRolls > 0:
		$ControlHUD/HUDrollButton.show()
		if theCurrentPlayer == 0:
			$computerTimer.start()
	else :
		$ControlHUD/HUDrollButton.hide()
		$updateTimer.start()

func _the_computer_decides():
	$computerTimer.stop()
	if hasCrew == 0:
		_roll_dice()
	else:
		if computerscore < computerThreshold :
			_roll_dice()
		else:
			$updateTimer.stop()
			$Timer.stop()
			_record_results()

func _update_score(currentScore):
	#print(theCurrentPlayer,currentScore)
	if theCurrentPlayer == 0 :
		computerscore = currentScore
		get_node("ControlHUD/HUDcomputerPoints").set("howManyPoints", currentScore)
	elif theCurrentPlayer == 1 :
		player1score = currentScore
		get_node("ControlHUD/HUDplayer1Points").set("howManyPoints", currentScore)
	elif theCurrentPlayer == 2 :
		player2score = currentScore
		get_node("ControlHUD/HUDplayer2Points").set("howManyPoints", currentScore)
	elif theCurrentPlayer == 3 :
		player3score = currentScore
		get_node("ControlHUD/HUDplayer3Points").set("howManyPoints", currentScore)
	elif theCurrentPlayer == 4 :
		player4score = currentScore
		get_node("ControlHUD/HUDplayer4Points").set("howManyPoints", currentScore)
	
	if currentScore == 12 :
		_rolled_midnight(theCurrentPlayer)

func _record_results() :
	$updateTimer.stop()
	theCurrentPlayer = theCurrentPlayer + 1
	if theCurrentPlayer > maxPlayers :
		_end_of_game()
	else :
		_next_player_setup()

func _end_of_game() :
	var winText = "winner"
	$ControlHUD.hide()
	$pirateShip.hide()
	$pirateHat.hide()
	$piratecrewHat.hide()
	$Timer.stop()
	$spinTimer.stop()
	$updateTimer.stop()
	$winControl/winMidnightLabel.hide()
	$winControl.show()
	
	if isItMidnight == 1 :
		$pocketwatch.show()
		$winControl/winMidnightLabel.show()
	else :
		$pocketwatch.hide()
		# How to determin the winner by using an Array.
		var winArray = [computerscore, player1score, player2score, player3score, player4score]
		print ("win array ", winArray)
		var winMax = winArray.max()
		if winArray.count(winMax) > 1 :
			# This means the high score is in there more than once, so it is a tie.
			whoWon = 10
		else :
			whoWon = winArray.find(winMax,0)
		
	print (whoWon, " won")
	if whoWon == 0 :
		winText = "The Computer wins!"
	elif whoWon == 1 :
		winText = "Player 1 wins!"
	elif whoWon == 2 :
		winText = "Player 2 wins!"
	elif whoWon == 3 :
		winText = "Player 3 wins!"
	elif whoWon == 4 :
		winText = "Player 4 wins!"
	elif whoWon > 4 :
		winText = "No Winner!"
	
	get_node("winControl/winnerLabel").set("winnerLabelText", winText)

# warning-ignore:unused_argument
func _process(delta):
	pass

func _rolled_midnight(midnightRoller) :
	$pocketwatch.show()
	print(midnightRoller, " Rolled Midnight!")
	isItMidnight = 1
	whoWon = midnightRoller
	theCurrentPlayer = 10
	_end_of_game()

func _on_HUDrollButton_pressed():
	if theCurrentPlayer != 0:
		if remainingRolls > 0 :
			$ControlHUD/HUDkeepButton.hide()
			_roll_dice()

func _on_HUDkeepButton_pressed():
	if theCurrentPlayer != 0:
		$updateTimer.stop()
		$Timer.stop()
		_record_results()

func _on_AreaA1_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceA = 1
		#print (diceA)

func _on_AreaA2_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceA = 2
		#print (diceA)

func _on_AreaA3_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceA = 3
		#print (diceA)

func _on_AreaA4_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceA = 4
		#print (diceA)

func _on_AreaA5_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceA = 5
		#print (diceA)

func _on_AreaA6_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceA = 6
		#print (diceA)

func _on_AreaB1_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceB = 1
		#print (diceB)

func _on_AreaB2_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceB = 2
		#print (diceB)

func _on_AreaB3_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceB = 3
		#print (diceB)

func _on_AreaB4_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceB = 4
		#print (diceB)

func _on_AreaB5_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceB = 5
		#print (diceB)

func _on_AreaB6_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceB = 6
		#print (diceB)

func _on_AreaC1_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceC = 1
		#print (diceC)

func _on_AreaC2_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceC = 2
		#print (diceC)

func _on_AreaC3_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceC = 3
		#print (diceC)

func _on_AreaC4_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceC = 4
		#print (diceC)

func _on_AreaC5_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceC = 5
		#print (diceC)

func _on_AreaC6_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceC = 6
		#print (diceC)

func _on_AreaD1_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceD = 1
		#print (diceD)

func _on_AreaD2_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceD = 2
		#print (diceD)

func _on_AreaD3_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceD = 3
		#print (diceD)

func _on_AreaD4_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceD = 4
		#print (diceD)

func _on_AreaD5_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceD = 5
		#print (diceD)

func _on_AreaD6_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceD = 6
		#print (diceD)

func _on_AreaE1_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceE = 1
		#print (diceE)

func _on_AreaE2_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceE = 2
		#print (diceE)

func _on_AreaE3_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceE = 3
		#print (diceE)

func _on_AreaE4_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceE = 4
		#print (diceE)

func _on_AreaE5_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceE = 5
		#print (diceE)

func _on_AreaE6_body_entered(body):
	if body.name == "TableObject" :
		$diceSoundPlayer.play()
		diceE = 6
		#print (diceE)

func _on_Timer_timeout():
	_check_all_dice()


func _on_spinTimer_timeout():
	_spin_dice()


func _on_updateTimer_timeout():
	_record_results()


func _on_computerTimer_timeout():
	_the_computer_decides()
