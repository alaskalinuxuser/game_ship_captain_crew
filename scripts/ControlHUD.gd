extends Control


func _ready():
	# All of the scaling is handled in the GameTable.gd script.
	get_node("HUDplayer1Label").show()
	get_node("HUDplayer1Points").show()
	
	if get_node("/root/global").computerPlayer == 0 :
		get_node("HUDcomputerLabel").hide()
		get_node("HUDcomputerPoints").hide()
	else:
		get_node("HUDcomputerLabel").show()
		get_node("HUDcomputerPoints").show()
	
	if get_node("/root/global").numberOfPlayers == 4 :
		get_node("HUDplayer2Label").show()
		get_node("HUDplayer2Points").show()
		get_node("HUDplayer3Label").show()
		get_node("HUDplayer3Points").show()
		get_node("HUDplayer4Label").show()
		get_node("HUDplayer4Points").show()
	elif get_node("/root/global").numberOfPlayers == 3 :
		get_node("HUDplayer2Label").show()
		get_node("HUDplayer2Points").show()
		get_node("HUDplayer3Label").show()
		get_node("HUDplayer3Points").show()
		get_node("HUDplayer4Label").hide()
		get_node("HUDplayer4Points").hide()
	elif get_node("/root/global").numberOfPlayers == 2 :
		get_node("HUDplayer2Label").show()
		get_node("HUDplayer2Points").show()
		get_node("HUDplayer3Label").hide()
		get_node("HUDplayer3Points").hide()
		get_node("HUDplayer4Label").hide()
		get_node("HUDplayer4Points").hide()
	else :
		get_node("HUDplayer2Label").hide()
		get_node("HUDplayer2Points").hide()
		get_node("HUDplayer3Label").hide()
		get_node("HUDplayer3Points").hide()
		get_node("HUDplayer4Label").hide()
		get_node("HUDplayer4Points").hide()

# warning-ignore:unused_argument
func _process(delta):
	pass

func _on_HUDmainButton_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/LaunchMenu.tscn")


func _on_winMenuButton_pressed():
	_on_HUDmainButton_pressed()
