extends Label

var whoseTurnIsIt = 0
var currentTurn = "Computer"

func _ready():
	if whoseTurnIsIt == 0 :
		currentTurn = "Computer"
	elif whoseTurnIsIt == 1 :
		currentTurn = "Player 1"
	elif whoseTurnIsIt == 2 :
		currentTurn = "Player 2"
	elif whoseTurnIsIt == 3 :
		currentTurn = "Player 3"
	elif whoseTurnIsIt == 4 :
		currentTurn = "Player 4"
	text = String(currentTurn)


# warning-ignore:unused_argument
func _process(delta):
	_ready()
