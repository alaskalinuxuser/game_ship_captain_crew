extends Control


func _ready():
	# My way to change the scale for every screen size. This allows me to build
	# in 1920 x 1080 resolution, and scale it for bigger or smaller displays.
	# This works for both 16:9 and 4:3 screen ratios, although 4:3 may not have 
	# the proper look.
	# Tested satisfactory on: 800x480, 854x480, 800x600, 960x540, 1024x600, 1280x768, 1280x720, 1280x800, 1824x1200, 1920x1080
	# Did not work well on 1400x900.
	
	var new_y_scale = get_node("/root/global").my_viewport_scale
	var new_x_scale = new_y_scale
	
	if get_node("/root/global").actualScreenRatio < 1.4 :
		new_x_scale = new_y_scale * ((800/600) / get_node("/root/global").actualScreenRatio)
	elif get_node("/root/global").actualScreenRatio < 1.61 :
		new_x_scale = new_y_scale / (get_node("/root/global").actualScreenRatio/1.45)
	elif get_node("/root/global").actualScreenRatio < 1.76 :
		new_x_scale = new_y_scale / (get_node("/root/global").actualScreenRatio/1.54)
	else :
		new_x_scale = new_y_scale
	
	get_node("/root").get_children()[2].set_scale(Vector2(new_x_scale,new_y_scale))
	# print(get_node("/root").get_children()[1].get_scale()) # Reference only.
	
	

func _on_TitleQuitButton_pressed():
	get_tree().quit()


func _on_TitleAboutButton_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/AboutMenu.tscn")


func _on_TitleTutButton_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Tutorial.tscn")


func _on_TitlePlayButton_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/PlayerSelection.tscn")
