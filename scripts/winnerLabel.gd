extends Label
var winnerLabelText = "No Winner!"


func _ready():
	text = String(winnerLabelText)


# warning-ignore:unused_argument
func _process(delta):
	_ready()
