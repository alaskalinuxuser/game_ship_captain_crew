extends Node

var my_viewport_scale = 1
var actualScreenRatio = 1.777777
var numberOfPlayers = 1
var computerPlayer = 0

func _ready():
	randomize()
	var viewport = 0
	viewport = get_node("/root").get_children()[1].get_viewport_rect().size
	# comment the below call for OS.get_screen_size() if you want to force a
	# certain screen size. If this call is not commented out, it will use the
	# native screen size of the device or computer, unless it is greater than
	# the Godot project settings of 1920*1080.
	viewport = OS.get_screen_size()
	# print(viewport) # Reference only
	
	actualScreenRatio = viewport.x / viewport.y
	
	my_viewport_scale = viewport.y/1080
